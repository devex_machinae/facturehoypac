/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.tavech.plugin.pac.facturehoy;

import com.certus.facturehoy.ws2.cfdi.WsEmisionTimbrado;
import com.certus.facturehoy.ws2.cfdi.WsEmisionTimbradoService;
import com.certus.facturehoy.ws2.cfdi.WsResponseBO;
import java.util.List;
import mx.com.tavech.plugin.pac.PACOperations;
import mx.com.tavech.plugin.pac.PACResponse;

/**
 *
 * @author Sistemas
 */
public class FactureHoyOps implements PACOperations {
    
    public PACResponse emitirTimbrar(String user, String password, int idService, byte[] xml) {
        //Se instancia el Web Service y se ejecuta el método de timbrado
        WsEmisionTimbradoService service = new WsEmisionTimbradoService();
        WsEmisionTimbrado port = service.getWsEmisionTimbradoPort();
        WsResponseBO responseBO = port.emitirTimbrar(user, password, idService, xml);
        return parseResponsePAC(responseBO);
    }
    
    public PACResponse cancelarFacturas(String user, String password, String keyEncode, List<String> uuidStringList) {
        //Se instancia el Web Service y se ejecuta el método de Cancelar
        WsEmisionTimbradoService service = new WsEmisionTimbradoService();
        WsEmisionTimbrado port = service.getWsEmisionTimbradoPort();
        WsResponseBO responseBO = port.cancelar(user, password, uuidStringList, keyEncode);
        return parseResponsePAC(responseBO);
    }
    
    public PACResponse obtenerAcuse(String user, String password, String uuid) {
        //Se instancia el Web Service y se ejecuta el método de Cancelar
        WsEmisionTimbradoService service = new WsEmisionTimbradoService();
        WsEmisionTimbrado port = service.getWsEmisionTimbradoPort();
        WsResponseBO responseBO = port.recuperarAcuse(user, password, uuid);
        return parseResponsePAC(responseBO);
    }
    
    public PACResponse emitirTimbrarDemo(String user, String password, int idService, byte[] xml) {
        //Se instancia el Web Service y se ejecuta el método de timbrado
        com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbradoService service = new com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbradoService();
        com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbrado port = service.getWsEmisionTimbradoPort();
        com.certus.facturehoy.ws2.cfdi.test.WsResponseBO responseBO = port.emitirTimbrar(user, password, idService, xml);
        return parseResponsePAC(responseBO);
    }
    
    public PACResponse cancelarFacturasDemo(String user, String password, String keyEncode, List<String> uuidStringList) {
        //Se instancia el Web Service y se ejecuta el método de Cancelar
        com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbradoService service = new com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbradoService();
        com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbrado port = service.getWsEmisionTimbradoPort();
        com.certus.facturehoy.ws2.cfdi.test.WsResponseBO responseBO = port.cancelar(user, password, uuidStringList, keyEncode);
        return parseResponsePAC(responseBO);
    }
    
    public PACResponse obtenerAcuseDemo(String user, String password, String uuid) {
        //Se instancia el Web Service y se ejecuta el método de Cancelar
        com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbradoService service = new com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbradoService();
        com.certus.facturehoy.ws2.cfdi.test.WsEmisionTimbrado port = service.getWsEmisionTimbradoPort();
        com.certus.facturehoy.ws2.cfdi.test.WsResponseBO responseBO = port.recuperarAcuse(user, password, uuid);
        return parseResponsePAC(responseBO);
    }
    
    private PACResponse parseResponsePAC(com.certus.facturehoy.ws2.cfdi.test.WsResponseBO responseBO){
        PACResponse response = new PACResponse();
        response.setAcuse(responseBO.getAcuse());
        response.setArregloAcuse(responseBO.getArregloAcuse());
        response.setCadenaOriginal(responseBO.getCadenaOriginal());
        response.setFechaHoraTimbrado(responseBO.getFechaHoraTimbrado());
        response.setFolioUDDI(responseBO.getFolioUDDI());
        
        response.setIsError(responseBO.isIsError());
        response.setMessage(responseBO.getMessage());
        
        response.setRutaDescargaPDF(responseBO.getRutaDescargaPDF());
        response.setPdf(responseBO.getPDF());
        
        response.setRutaDescargaXML(responseBO.getRutaDescargaXML());
        response.setXml(responseBO.getXML());
        
        response.setSelloDigitalEmisor(responseBO.getSelloDigitalEmisor());
        response.setSelloDigitalTimbreSAT(responseBO.getSelloDigitalTimbreSAT());
        return response;
    }
    
    private PACResponse parseResponsePAC(WsResponseBO responseBO){
        PACResponse response = new PACResponse();
        response.setAcuse(responseBO.getAcuse());
        response.setArregloAcuse(responseBO.getArregloAcuse());
        response.setCadenaOriginal(responseBO.getCadenaOriginal());
        response.setFechaHoraTimbrado(responseBO.getFechaHoraTimbrado());
        response.setFolioUDDI(responseBO.getFolioUDDI());
        
        response.setIsError(responseBO.isIsError());
        response.setMessage(responseBO.getMessage());
        
        response.setRutaDescargaPDF(responseBO.getRutaDescargaPDF());
        response.setPdf(responseBO.getPDF());
        
        response.setRutaDescargaXML(responseBO.getRutaDescargaXML());
        response.setXml(responseBO.getXML());
        
        response.setSelloDigitalEmisor(responseBO.getSelloDigitalEmisor());
        response.setSelloDigitalTimbreSAT(responseBO.getSelloDigitalTimbreSAT());
        return response;
    }
    
}
