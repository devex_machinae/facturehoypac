/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.tavech.plugin.pac;

import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Sistemas
 */
public class PACResponse {
    
    private byte[] pdf;
    private byte[] xml;
    private byte[] acuse;
    private List<byte[]> arregloAcuse;
    
    private String selloDigitalEmisor;
    private String selloDigitalTimbreSAT;
    
    private String rutaDescargaPDF;
    private String rutaDescargaXML;
    
    private XMLGregorianCalendar fechaHoraTimbrado;
    private String cadenaOriginal;
    
    private String folioUDDI;
    private Boolean isError;
    private String message;

    /**
     * @return the pdf
     */
    public byte[] getPdf() {
        return pdf;
    }

    /**
     * @param pdf the pdf to set
     */
    public void setPdf(byte[] pdf) {
        this.pdf = pdf;
    }

    /**
     * @return the xml
     */
    public byte[] getXml() {
        return xml;
    }

    /**
     * @param xml the xml to set
     */
    public void setXml(byte[] xml) {
        this.xml = xml;
    }

    /**
     * @return the acuse
     */
    public byte[] getAcuse() {
        return acuse;
    }

    /**
     * @param acuse the acuse to set
     */
    public void setAcuse(byte[] acuse) {
        this.acuse = acuse;
    }

    /**
     * @return the arregloAcuse
     */
    public List<byte[]> getArregloAcuse() {
        return arregloAcuse;
    }

    /**
     * @param arregloAcuse the arregloAcuse to set
     */
    public void setArregloAcuse(List<byte[]> arregloAcuse) {
        this.arregloAcuse = arregloAcuse;
    }

    /**
     * @return the selloDigitalEmisor
     */
    public String getSelloDigitalEmisor() {
        return selloDigitalEmisor;
    }

    /**
     * @param selloDigitalEmisor the selloDigitalEmisor to set
     */
    public void setSelloDigitalEmisor(String selloDigitalEmisor) {
        this.selloDigitalEmisor = selloDigitalEmisor;
    }

    /**
     * @return the selloDigitalTimbreSAT
     */
    public String getSelloDigitalTimbreSAT() {
        return selloDigitalTimbreSAT;
    }

    /**
     * @param selloDigitalTimbreSAT the selloDigitalTimbreSAT to set
     */
    public void setSelloDigitalTimbreSAT(String selloDigitalTimbreSAT) {
        this.selloDigitalTimbreSAT = selloDigitalTimbreSAT;
    }

    /**
     * @return the rutaDescargaPDF
     */
    public String getRutaDescargaPDF() {
        return rutaDescargaPDF;
    }

    /**
     * @param rutaDescargaPDF the rutaDescargaPDF to set
     */
    public void setRutaDescargaPDF(String rutaDescargaPDF) {
        this.rutaDescargaPDF = rutaDescargaPDF;
    }

    /**
     * @return the rutaDescargaXML
     */
    public String getRutaDescargaXML() {
        return rutaDescargaXML;
    }

    /**
     * @param rutaDescargaXML the rutaDescargaXML to set
     */
    public void setRutaDescargaXML(String rutaDescargaXML) {
        this.rutaDescargaXML = rutaDescargaXML;
    }

    /**
     * @return the fechaHoraTimbrado
     */
    public XMLGregorianCalendar getFechaHoraTimbrado() {
        return fechaHoraTimbrado;
    }

    /**
     * @param fechaHoraTimbrado the fechaHoraTimbrado to set
     */
    public void setFechaHoraTimbrado(XMLGregorianCalendar fechaHoraTimbrado) {
        this.fechaHoraTimbrado = fechaHoraTimbrado;
    }

    /**
     * @return the cadenaOriginal
     */
    public String getCadenaOriginal() {
        return cadenaOriginal;
    }

    /**
     * @param cadenaOriginal the cadenaOriginal to set
     */
    public void setCadenaOriginal(String cadenaOriginal) {
        this.cadenaOriginal = cadenaOriginal;
    }

    /**
     * @return the folioUDDI
     */
    public String getFolioUDDI() {
        return folioUDDI;
    }

    /**
     * @param folioUDDI the folioUDDI to set
     */
    public void setFolioUDDI(String folioUDDI) {
        this.folioUDDI = folioUDDI;
    }

    /**
     * @return the isError
     */
    public Boolean getIsError() {
        return isError;
    }

    /**
     * @param isError the isError to set
     */
    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
