/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.tavech.plugin.pac;

import java.util.List;

/**
 *
 * @author Sistemas
 */
public interface PACOperations {
    
    PACResponse emitirTimbrar(String user, String password, int idService, byte[] xml);
    
    PACResponse cancelarFacturas(String user, String password, String keyEncode, List<String> uuidStringList);
    
    PACResponse obtenerAcuse(String user, String password, String uuid);
    
    PACResponse emitirTimbrarDemo(String user, String password, int idService, byte[] xml);
    
    PACResponse cancelarFacturasDemo(String user, String password, String keyEncode, List<String> uuidStringList);
    
    PACResponse obtenerAcuseDemo(String user, String password, String uuid);
    
}
