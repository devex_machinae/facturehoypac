/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.tavech.plugin.pac;

import java.io.ByteArrayInputStream;
import java.security.PrivateKey;
import mx.bigdata.sat.security.KeyLoader;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Sistemas
 */
public class Encoder {
    
    public static String EncodKeyBytesToB64(byte[] keyFile,String keyPass) throws Exception{
        //se obtiene objeto private key a partir del File
        PrivateKey key = KeyLoader.loadPKCS8PrivateKey(new ByteArrayInputStream(keyFile),keyPass);
        //Se obtiene y codifica en Base64 el key del CSD
        BASE64Encoder b64 = new BASE64Encoder();
        String encode = b64.encode(key.getEncoded());
        return encode;
    }
}
